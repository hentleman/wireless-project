/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 * Copyright (c) 2013 Budiarto Herman
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Original work authors (from a2-a4-rsrq-handover-algorithm.h):
 * - Budiarto Herman <budiarto.herman@magister.fi>
 *
 * Converted to handover algorithm interface by:
 * - codeonwort <alstjr5301@naver.com>
 */

#ifndef CUSTOM_HANDOVER_ALGORITHM_H
#define CUSTOM_HANDOVER_ALGORITHM_H

#include <ns3/lte-handover-algorithm.h>
#include <ns3/lte-handover-management-sap.h>
#include <ns3/lte-rrc-sap.h>
#include <ns3/simple-ref-count.h>
#include <ns3/ptr.h>
#include <map>

using namespace std;

typedef uint16_t RsrqKey;
typedef struct { double TTTstart; unsigned int count; double accum; } RsrqValue;
typedef map<RsrqKey, RsrqValue> RsrqMap;

namespace ns3 {


/**
 * \brief Handover algorithm implementation based on averaged RSRQ measurements
 *
 * This implementation is based on 'LTE Hard Handover Algorithm with Average
 * RSRP Constraint' which is represented in the paper 'Optimized Performance
 * Evaluation of LTE Hard Handover Algorithm with Average RSRP Constraint.'
 *
 * Handover decision made by this algorithm is based on three conditions.
 * 1. RSRQ_target - RSRQ_current >= HOM
 * 2. TTT >= elapsed time since condition 1 is fulfilled.
 * 3. RSRQ_target >= RSRQ_current_average
 *
 * When all three conditions above are fulfilled, the algorithm
 * informs the eNodeB RRC to trigger a handover.
 *
 * The HOM(threshold) can be configured in the `Threshold` attribute.
 * The TTT can also be configured in the 'TTT' attribute.
 *
 * The following code snippet is an example of using and configuring the
 * handover algorithm in a simulation program:
 *
 *     Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
 *
 *     NodeContainer enbNodes;
 *     // configure the nodes here...
 *
 *     lteHelper->SetHandoverAlgorithmType ("ns3::CustomHandoverAlgorithm");
 *     lteHelper->SetHandoverAlgorithmAttribute ("Threshold",
 *                                               UintegerValue (30));
 *     lteHelper->SetHandoverAlgorithmAttribute ("TTT",
 *                                               DoubleValue (1));
 *     NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
 *
 * \note Setting the handover algorithm type and attributes after the call to
 *       LteHelper::InstallEnbDevice does not have any effect to the devices
 *       that have already been installed.
 */
class CustomHandoverAlgorithm : public LteHandoverAlgorithm
{
public:
  /// Creates an custom handover algorithm instance.
  CustomHandoverAlgorithm ();

  virtual ~CustomHandoverAlgorithm ();

  // inherited from Object
  static TypeId GetTypeId ();

  // inherited from LteHandoverAlgorithm
  virtual void SetLteHandoverManagementSapUser (LteHandoverManagementSapUser* s);
  virtual LteHandoverManagementSapProvider* GetLteHandoverManagementSapProvider ();

  // let the forwarder class access the protected and private members
  friend class MemberLteHandoverManagementSapProvider<CustomHandoverAlgorithm>;

protected:
  // inherited from Object
  virtual void DoInitialize ();
  virtual void DoDispose ();

  // inherited from LteHandoverAlgorithm as a Handover Management SAP implementation
  void DoReportUeMeas (uint16_t rnti, LteRrcSap::MeasResults measResults);

private:
  /**
   * Called when Event A2 is detected, then trigger a handover if needed.
   *
   * \param rnti The RNTI of the UE who reported the event.
   * \param servingCellRsrq The RSRQ of this cell as reported by the UE.
   */
  void EvaluateHandover (uint16_t rnti, uint8_t servingCellRsrq);

  /**
   * Determines if a neighbour cell is a valid destination for handover.
   * Currently always return true.
   *
   * \param cellId The cell ID of the neighbour cell.
   * \return True if the cell is a valid destination for handover.
   */
  bool IsValidNeighbour (uint16_t cellId);

  /**
   * Called when Event A4 is reported, then update the measurements table.
   * If the RNTI and/or cell ID is not found in the table, a corresponding
   * entry will be created. Only the latest measurements are stored in the
   * table.
   *
   * \param rnti The RNTI of the UE who reported the event.
   * \param cellId The cell ID of the measured cell.
   * \param rsrq The RSRQ of the cell as measured by the UE.
   */
  void UpdateNeighbourMeasurements (uint16_t rnti, uint16_t cellId,
                                    uint8_t rsrq);

  /// The expected measurement identity for A2 measurements.
  uint8_t m_a2MeasId;
  /// The expected measurement identity for A4 measurements.
  uint8_t m_a4MeasId;

  /**
   * Measurements reported by a UE for a cell ID. The values are quantized
   * according 3GPP TS 36.133 section 9.1.4 and 9.1.7.
   */
  class UeMeasure : public SimpleRefCount<UeMeasure>
  {
  public:
    uint16_t m_cellId;  ///< Cell ID.
    uint8_t m_rsrp;     ///< RSRP in quantized format. \todo Can be removed?
    uint8_t m_rsrq;     ///< RSRQ in quantized format.
  };

  /**
   * Measurements reported by a UE for several cells. The structure is a map
   * indexed by the cell ID.
   */
  typedef std::map<uint16_t, Ptr<UeMeasure> > MeasurementRow_t;

  /**
   * Measurements reported by several UEs. The structure is a map indexed by
   * the RNTI of the UE.
   */
  typedef std::map<uint16_t, MeasurementRow_t> MeasurementTable_t;

  /// Table of measurement reports from all UEs.
  MeasurementTable_t m_neighbourCellMeasures;

  /**
   * The `Threshold` attribute. If the RSRQ of the serving cell is
   * worse than this threshold, neighbour cells are consider for handover.
   * Expressed in quantized range of [0..34] as per Section 9.1.7 of
   * 3GPP TS 36.133.
   */
  uint8_t m_threshold;

  /**
   * The `TTT` attribute. Minimum offset between the serving
   * and the best neighbour cell to trigger the handover. Expressed in
   * quantized range of [0..34] as per Section 9.1.7 of 3GPP TS 36.133.
   */
  double m_TTT;

  int m_currentCount;
  double m_rsrqAccum;
  RsrqMap m_rsrqMap;

  /// Interface to the eNodeB RRC instance.
  LteHandoverManagementSapUser* m_handoverManagementSapUser;
  /// Receive API calls from the eNodeB RRC instance.
  LteHandoverManagementSapProvider* m_handoverManagementSapProvider;

}; // end of class CustomHandoverAlgorithm


} // end of namespace ns3


#endif /* A2_A4_RSRQ_HANDOVER_ALGORITHM_H */
